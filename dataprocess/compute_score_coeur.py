# -*- coding: utf-8 -*-

import pdb
import numpy as np
import datetime as dt
import swifter
from settings import *


def f_ln_bili_la(bili, dt_bili):
    """
    Fonction Bilirubine en Liste d’attente
    :param bili: float or None: valeur de la mesure
    :param dt_bili: datetime: date de la mesure
    :return: float: compris entre ln(5) et ln(230)
    """
    if (bili is None) or ((cfg['score']['today'] - dt_bili) > cfg['score']['delay_bili']):
        return np.log(5)
    return np.log(min(230, max(5, bili)))

def f_ln_dfg_la(dialyse, creat, dt_creat, age_r, sex_r):
    """
     Fonction Débit de Filtration Glomérulaire en Liste d’attente (méthode MDRD)
    :param dialyse: bool : présence d'une dialyse ?
    :param creat: float or None: valeur de la créatine ?
    :param dt_creat: datetime: date de la mesure de créatine
    :param age_r: int: age du receveur
    :param sex_r: 'F' ou autre chose: sexe du receveur
    :return: float: compris entre 0 et ln(150)
    """
    if dialyse:
        return np.log(15)
    if (creat is None) or ((cfg['score']['today'] - dt_creat) > cfg['score']['delay_la']):
        return np.log(150)
    f_dfg = 186.3 * (creat / 88.4)**(-1.154) * (age_r**(-0.203))
    f_dfg *= (1 - 0.258 * (sex_r=='F'))
    return np.log(
        min(150, max(1, f_dfg))
    )

def f_decile_pn(cec, cat, siav, bnp, probnp, dt_probnp, dt_bnp):
    """
    Fonction décile des peptides natriurétiques (BNP ou NT-ProBNP)
    :param cec: bool: lien avec assitance de courte durée
    :param cat: bool: ??
    :param siav: 'B' ou autre chose: ??
    :param bnp: int or None: Peptides natriurétiques
    :param probnp: int or None: Peptides natriurétiques (autre méthode de dosage ?)
    :param dt_probnp: datetime: date de la mesure de probnp
    :param dt_bnp: datetime: date de la mesure de bnp
    :return: int: entre 1 et 10
    """

    if cec or cat or siav=='B':
        return 10
    if (bnp is None) and (probnp is None):
        return 1
    if isinstance(dt_probnp, dt.datetime):
        if (probnp is not None) and ((cfg['score']['today'] - dt_probnp) <= cfg['score']['delay_probnp']):
            if probnp < 928:
                return 1
            if probnp < 1478:
                return 2
            if probnp < 2044:
                return 3
            if probnp < 2661:
                return 4
            if probnp < 3416:
                return 5
            if probnp < 4406:
                return 6
            if probnp < 5645:
                return 7
            if probnp < 8000:
                return 8
            if probnp < 11332:
                return 9
            if probnp >= 11332:
                return 10
    if isinstance(dt_bnp, dt.datetime):
        if (bnp is not None) and ((cfg['score']['today'] - dt_bnp) <= cfg['score']['delay_bnp']):
            if bnp < 189:
                return 1
            if bnp < 314:
                return 2
            if bnp < 481:
                return 3
            if bnp < 622:
                return 4
            if bnp < 818:
                return 5
            if bnp < 1074:
                return 6
            if bnp < 1317:
                return 7
            if bnp < 1702:
                return 8
            if bnp < 2696:
                return 9
            if bnp >= 2696:
                return 10

    return 1


def f_icar(arg_dict):
    """
    Calcul de l'index de risque cardiaque (0:40). Il prend en compte le risque de deces
    en liste d'attente.
    :param arg_dict: dictionary containing all the variables
    :return: float : index de risque cardiaque entre 0 et 40.
    """

    # étapes intermédiaires pour le calcul de la fonction de risque pré-greffe

    ascd = int(arg_dict['cec'])
    decil_pn_args = {
        key: arg_dict[key]
            for key in ['cec', 'cat', 'siav', 'bnp', 'probnp', 'dt_probnp', 'dt_bnp']
    }
    decile_pn = f_decile_pn(**decil_pn_args)
    dfg_la_args = {
        key: arg_dict[key]
            for key in ['dialyse', 'creat', 'dt_creat', 'age_r', 'sex_r']
    }
    ln_dfg_la = f_ln_dfg_la(**dfg_la_args)
    bili_la_args = {
        key: arg_dict[key]
        for key in ['bili', 'dt_bili']
    }
    ln_bili_la = f_ln_bili_la(**bili_la_args)

    # calcul de la fonction de risque pré-greffe en liste d'attente
    risque_pre_grf = 1.301335 * ascd
    risque_pre_grf += 0.157691 * decile_pn
    risque_pre_grf -= 0.510058 * ln_dfg_la
    risque_pre_grf += 0.615711 * ln_bili_la

    # constante de l'ICAR
    c_icar = 0.157691 * 1 - 0.510058 * np.log(150) + 0.615711 * np.log(5)

    icar = min(40, max(0,
                       round((risque_pre_grf - c_icar)*10)
                       ))

    return icar


def f_icar_max(arg_dict):
    """
    Renvoie le maximum entre l'ICAR à date et l'ICAR avant injection ou mise sous circulation
    """
    icar_j = f_icar(arg_dict)
    if not arg_dict['cec'] and not arg_dict['drg']:
        return icar_j
    else:
        arg_dict_i = arg_dict.copy()
        for key in ['bnp_avi', 'probnp_avi', 'dialyse_avi', 'creat_avi', 'bili_avi']:
            new_key = key[:-4]
            arg_dict_i[new_key] = arg_dict.pop(key)
        for key in ['dt_bnp', 'dt_probnp', 'dt_creat', 'dt_bili']:
            arg_dict_i[key] = cfg['score']['today']
        # print(arg_dict_i)
        icar_i = f_icar(arg_dict_i)
        return max(icar_j, icar_i)


def f_comp_ascd(arg_dict):
    """
    Diminution des points après la mise en place d'une CEC temporaire
    """
    if arg_dict['delay_cec'] < cfg['score']['threshold_cec_1']:
        return 1
    if arg_dict['delay_cec'] < cfg['score']['threshold_cec_2']:
        return 1 - cfg['score']['decote_cec'] * (arg_dict['delay_cec'] - (cfg['score']['threshold_cec_1'] - 1))
    else:
        return 0

def f_ccb(arg_dict):
    """
    Le score cardiaque composite brut.
    :param icar: index de risque cardiaque
    :param ad_std: Composante Adulte Standard
    :param ad_xpr: Composante Expert Adulte(XPCA)
    :param ped_std: Composante Pédiatrique Standard
    :param ped_xpr: Composante Expert Pédiatrique(XPCP)
    :return:
    """
    # ICAR standardisé (entre 0 et 1000)
    icar_std = f_icar_max(arg_dict) * 1000. / 40.

    if arg_dict['cec']:
        icar_std *= f_comp_ascd(arg_dict)

    if icar_std >= 775:
        icar_std = icar_std + 51

    # composante standard adulte
    if (arg_dict['age_r'] >= 18) and (arg_dict['urgence'] is None):
        return icar_std

    # composante pédiatrique standard
    if (arg_dict['age_r'] < 18) and (arg_dict['urgence'] is None):
        return 775 + 50. * max(0, min(1, arg_dict['da'] / 24.))

    # composante expert adulte
    if (arg_dict['age_r'] >= 18) and (arg_dict['urgence']=='XPCA'):
        if arg_dict['xpc']==0:
            return max(icar_std, 900)
        else:
            delayed_kxpc = 900 * max(0, min(1, float(arg_dict['daurg']) / float(arg_dict['xpc'])))
            return max(icar_std, delayed_kxpc)

    # composante expert enfant
    if (arg_dict['age_r'] < 18) and (arg_dict['urgence'] in ['XPCP1', 'XPCP2']):
        if arg_dict['urgence']=='XPCP1':
            kxpc = 1102
        if arg_dict['urgence']=='XPCP2':
            kxpc = 1051
        return kxpc + 50 * max(0, min(1, arg_dict['daurg'] / 30. / 24.))
    else:
        raise ValueError("Arguments of the f_ccb are invalid, no condition is matched")

def f_diff_age(age_r, age_d):
    d_age = age_r - age_d
    if d_age < 0:
        fo_diff_age = (d_age + 65.) / 25.
    else:
        fo_diff_age = 1 - (d_age - 15.) / 25.
    if age_r >= 18:
        return min(1, max(0, fo_diff_age))
    else:
        return 1

def f_s_corpo(taille, poids):
    """
    Formule pour calculer la surface corporelle d'un patient
    """
    return 0.007184 * taille**(0.725) * poids**(0.425)

def f3_sc(age_r, sc_r, sc_d, sex_d, poids_d):
    """
    Fonction de prise en compte des différences de surface corporelle entre donneur et receveur
    """
    if age_r >= 18:
        if (.8 * sc_r < sc_d) or (sex_d=='H' and poids_d >= 70):
            return 1
        else:
            return 0
    else:
        if ((.8 * sc_r < sc_d) and (2. * sc_r > sc_d)) or (sex_d=='H' and poids_d >= 70):
            return 1
        else:
            return 0

def f_risk_post_gref(arg_dict):
    """
    Calcul du risque post greffe
    """
    age_r_bool = int(arg_dict['age_r'] > 50)

    mal_bool = bool(arg_dict['mal_set'].intersection(set([162, 163, 815])))  # return True is at least one mal is in the list else False

    if (arg_dict['bili'] is None) or ((cfg['score']['today'] - arg_dict['dt_bili']) > cfg['score']['delay_bili']):
        ln_bili_grf = np.log(230)
    else:
        ln_bili_grf = np.log(min(230, max(5, arg_dict['bili'])))

    if arg_dict['dialyse']:
        ln_dfg_grf = np.log(15)
    elif (arg_dict['creat'] is None) or ((cfg['score']['today'] - arg_dict['dt_creat']) > cfg['score']['delay_la']):
        ln_dfg_grf = np.log(1)
    else:
        f_dfg = 186.3 * (arg_dict['creat']/ 88.4)**(-1.154) * (arg_dict['age_r']**(-0.203))
        f_dfg *= (1 - 0.258 * (arg_dict['sex_r']=='F'))
        ln_dfg_grf = np.log(min(150, max(1, f_dfg)))

    sex_bool = int(arg_dict['sex_d']=='F' and arg_dict['sex_r']=='H')

    age_d_bool = int(arg_dict['age_d']> 55)

    risk_post_gref = 0.50608 * age_r_bool
    risk_post_gref += 0.50754 * mal_bool
    risk_post_gref += 0.40268 * ln_bili_grf
    risk_post_gref -= 0.54443 * ln_dfg_grf
    risk_post_gref += 0.36262 * sex_bool
    risk_post_gref += 0.41714 * age_d_bool
    return risk_post_gref

def f_ccp(arg_dict):
    """
    Calcul du Score Composite Pondéré. Application au Score
    Cardiaque Composite Brut (Score CCB) d’un ensemble de filtres et de fonctions
    d’appariement donneur receveur. Ces filtres s’appliquent lors de la proposition
    du greffon cardiaque.
    :param ccb: score cardiaque composite brut
    :param age_diff: différence d’âge entre le receveur et le donneur
    :param abo_comp: compatibilité ABO entre receveur et donneur
    :param morpho_comp: compatibilité morphologique entre donneur et receveur
    :param filtre_efficicaite: iltre d’efficacité en terme de résultats attendus de la greffe cardiaque
    :return:
    """
    # valeur du score composite brut du receveur
    ccb = f_ccb(arg_dict)

    # appariement age
    coef_age_diff = f_diff_age(arg_dict['age_r'], arg_dict['age_d'])

    # appariement groupe sanguin
    coef_abo = int(
        (arg_dict['abo_r']==arg_dict['abo_d'])
        or (arg_dict['abo_d']=='A' and arg_dict['abo_r']=='AB')
        or (arg_dict['abo_d']=='O' and arg_dict['abo_r']=='B')
    ) + int(arg_dict['abo_d']=='B' and arg_dict['abo_r']=='AB') * 0.1


    # compatibilité morphologique
    s_corpo_r = f_s_corpo(arg_dict['taille_r'], arg_dict['poids_r'])
    s_corpo_d = f_s_corpo(arg_dict['taille_d'], arg_dict['poids_d'])
    coef_s_corpo = f3_sc(arg_dict['age_r'], s_corpo_r, s_corpo_d, arg_dict['sex_d'], arg_dict['poids_d'])

    # filtre efficacité en terme de résultats attendus de la greffe
    risk_post_grf = f_risk_post_gref(arg_dict)
    coef_surv_post_grf = 0.6785748856**(np.exp(risk_post_grf))

    f4_surv_post_grf = int(coef_surv_post_grf>0.5 or arg_dict['age_r']<18)

    return ccb * coef_age_diff * coef_abo * coef_s_corpo * f4_surv_post_grf


def f_snacg(arg_dict, safe=False):
    """
    Calcul du score national d'attribution des greffons cardiaques.
    :param ccp: score composite pondéré
    :param ttlgp: durée du trajet entre les lieux de prélèvement et de greffe
    :return:
    """
    if safe:
        try:
            ccp = f_ccp(arg_dict)
            mg = 1. / np.exp(0.0000002 * arg_dict['ttlgp'] ** 2.9)
            return ccp * mg
        except:
            return np.nan
    else:
        ccp = f_ccp(arg_dict)
        mg = 1. / np.exp(0.0000002 * arg_dict['ttlgp']**2.9)
        return ccp * mg


def compute_sc_df(pat_df, safe=False):
    return pat_df.apply(lambda row: f_snacg(row, safe=safe), axis=1)
