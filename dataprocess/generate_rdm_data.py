import numpy as np
import datetime as dt
import pandas as pd
from settings import *

def create_rdm_data_giver(size=1):
    if size==1:
        arg_dict = {}
        arg_dict['age_d'] = np.random.randint(1, 100) # int : age du donneur
        arg_dict['sex_d'] = np.random.choice(a=['H', 'F']) # 'F' ou 'H' sexe du donneur
        arg_dict['taille_d'] = abs(np.random.normal(170, 15))  #  taille du donneur en centimètre
        arg_dict['poids_d'] = abs(np.random.normal(61, 22))  #  poids du donneur en kilogramme
        arg_dict['abo_d'] = np.random.choice(a=['A', 'B', 'O', 'AB'], p=[.45, .09, .42, .04]) # groupe sanguin du donneur
        return arg_dict
    else:
        arg_dict = {}
        arg_dict['age_d'] = np.random.randint(1, 100, size=size)  # int : age du donneur
        arg_dict['sex_d'] = np.random.choice(a=['H', 'F'], size=size)  # 'F' ou 'H' sexe du donneur
        arg_dict['taille_d'] = abs(np.random.normal(170, 15, size=size))  # taille du donneur en centimètre
        arg_dict['poids_d'] = abs(np.random.normal(61, 22, size=size))  # poids du donneur en kilogramme
        arg_dict['abo_d'] = np.random.choice(a=['A', 'B', 'O', 'AB'],
                                             p=[.45, .09, .42, .04], size=size)  # groupe sanguin du donneur
        return arg_dict

def create_rdm_data_recipient(size):
    arg_dict = {}
    arg_dict['cec'] = np.random.choice(a=[False, True], size=size, p=[.9, .1])        # bool lien avec assistance mechanique
    arg_dict['delay_cec'] = np.random.uniform(0, 50, size=size)
    arg_dict['cat'] = np.random.choice(a=[False, True], size=size, p=[.9, .1])               # bool ??
    arg_dict['drg'] = np.random.choice(a=[False, True], size=size, p=[.8, .2])               # bool ??
    arg_dict['siav'] = np.random.choice(a=[None, 'B'], size=size, p=[.9, .1])              # 'B' ou autre chose
    arg_dict['bnp'] = np.random.randint(150, 3000, size=size)  # valeur de la mesure BNP
    arg_dict['bnp_avi'] = np.random.randint(150, 3000, size=size)  # valeur de la mesure BNP
    arg_dict['probnp'] = np.random.randint(900, 12000, size=size) # valeur de la mesure ProBNP
    arg_dict['probnp_avi'] = np.random.randint(900, 12000, size=size) # valeur de la mesure ProBNP avant injection
    arg_dict['dt_probnp'] = [dt.date.today() - dt.timedelta(days=np.random.randint(90)) for _ in range(size)] # date de la dernière mesure ProBNP
    arg_dict['dt_bnp'] = [dt.date.today() - dt.timedelta(days=np.random.randint(90)) for _ in range(size)] # date de la dernière mesure BNP
    arg_dict['dialyse'] = np.random.choice(a=[False, True], size=size, p=[.9, .1])  # bool : présence d'une dialyse ?
    arg_dict['dialyse_avi'] = np.random.choice(a=[False, True], size=size, p=[.9, .1])  # bool : présence d'une dialyse ?
    arg_dict['creat'] = np.random.uniform(25, 222, size=size) # mesure de la creatinine
    arg_dict['creat_avi'] = np.random.uniform(25, 222, size=size) # mesure de la creatinine avant injection
    arg_dict['dt_creat'] = [dt.date.today() - dt.timedelta(days=np.random.randint(90)) for _ in range(size)] # date de la dernière mesure de creatinine
    arg_dict['age_r'] = np.random.randint(1, 100, size=size) # int : age du receveur
    arg_dict['sex_r'] = np.random.choice(a=['H', 'F'], size=size) # 'F' ou 'H' sexe du receveur
    arg_dict['bili'] = np.random.randint(2, 60, size=size) # mesure de la Bilirubine
    arg_dict['bili_avi'] = np.random.randint(2, 60, size=size) # mesure de la Bilirubine avant injection
    arg_dict['dt_bili'] = [dt.date.today() - dt.timedelta(days=np.random.randint(90)) for _ in range(size)] # date de la dernière mesure de Bilirubine
    array = []
    val = np.random.choice(a=[0, 1, 2], p=[.9, .03, .07], size=size)
    for (urg, age) in zip(val, arg_dict['age_r']):
        if urg == 0:
            array.append(None)
        elif age >= 18:
            array.append('XPCA')
        elif urg == 1:
            array.append('XPCP1')
        else:
            array.append('XPCP2')
    arg_dict['urgence'] = array
    arg_dict['da'] = np.random.randint(0,36, size=size) # int : temps d'attente sur liste en mois (??)
    arg_dict['xpc'] = np.random.choice(a=[0, 90], size=size) # int délais d'attente avant obtention des points urgences en jours
    arg_dict['daurg'] = np.random.randint(0, 120, size=size) # int : temps d'attente depuis le passage en urgence en jours (??)
    arg_dict['taille_r'] = abs(np.random.normal(170, 15, size=size)) # taille du receveur en centimètre
    arg_dict['poids_r'] = abs(np.random.normal(61, 22, size=size)) #  poids du receveur en kilogramme
    arg_dict['mal_set'] = [set(l) for l in np.random.choice(a=cfg['score']['list_code_mal_init'], size=(size,3))] # code cristal maladie initiale du receveur 1
    arg_dict['ttlgp'] = np.random.uniform(0, 300, size=size) # durée de trajet entre les lieux de prélèvement et de greffe
    arg_dict['abo_r'] = np.random.choice(a=['A', 'B', 'O', 'AB'], p=[.45, .09, .42, .04], size=size) # groupe sanguin du receveur

    return arg_dict


def create_rdm_pat_db_one_giver(size):
    # create one giver for all the recipients candidates
    giver = create_rdm_data_giver()
    recipients = create_rdm_data_recipient(size)
    rdm_db = pd.DataFrame.from_dict(recipients)
    for (key, val) in giver.items():
        rdm_db[key] = val
    return rdm_db


def create_rdm_patient_db_from_giver(size, giver):
    recipients = create_rdm_data_recipient(size)
    rdm_db = pd.DataFrame.from_dict(recipients)
    for (key, val) in giver.items():
        rdm_db[key] = val
    return rdm_db


def create_pat_db(size):
    # create one giver for all the recipients candidates
    giver = create_rdm_data_giver(size)
    recipients = create_rdm_data_recipient(size)
    recipients.update(giver)
    rdm_db = pd.DataFrame.from_dict(recipients)
    return rdm_db
