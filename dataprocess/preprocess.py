import time
import datetime as dt
import pdb
from settings import *
import pandas as pd


EXCLUDED_VARIABLES = ['age_d', 'sex_d', 'taille_d', 'poids_d', 'abo_d',
                      'delay_probnp', 'delay_bnp', 'delay_la', 'delay_bili']

def compute_std(data):
    data_copy = data.copy()
    for col in data_copy.columns:
        if type(data_copy[col].head(1).values[0]) in [dt.date]:
            data_copy.loc[:, col] = data_copy[col] - dt.date(2000,1,1)
    return data_copy.std()


def var_selection(patient_df):
    col_sel = [col for col in patient_df.columns if col not in EXCLUDED_VARIABLES]
    return patient_df[col_sel]


def one_hot_encode(patient_df):
    flat_pat_df = patient_df.copy()

    # One hot encoding disease
    for mal in cfg['score']['list_code_mal_init']:
        flat_pat_df.loc[:, mal] = (pd.DataFrame(flat_pat_df.mal_set.tolist())==mal).any(1)

    # delete old representation of diseases
    del flat_pat_df['mal_set']

    if 'urgence' in flat_pat_df.columns:
        # setting 0, 1, 2 to urgence (only pediatrics can obtain 2)
        flat_pat_df.loc[flat_pat_df['urgence'].isnull(), 'urgence'] = 0
        flat_pat_df.loc[flat_pat_df['urgence'].astype(str)=='XPCA', 'urgence'] = 1
        flat_pat_df.loc[flat_pat_df['urgence'].astype(str)=='XPCP1', 'urgence'] = 1
        flat_pat_df.loc[flat_pat_df['urgence'].astype(str)=='XPCP2', 'urgence'] = 2

    # one hot encoding ABO groups
    for grp in ['A', 'B', 'O', 'AB']:
        if 'abo_r' in flat_pat_df.columns:
            flat_pat_df.loc[:, 'abo_r_' + grp] = (flat_pat_df['abo_r']==grp)
        if 'abo_d' in flat_pat_df.columns:
            flat_pat_df.loc[:, 'abo_d_' + grp] = (flat_pat_df['abo_d']==grp)

    # delete old representation of ABO groups
    if 'abo_d' in flat_pat_df.columns:
        del flat_pat_df['abo_d']
    if 'abo_r' in flat_pat_df.columns:
        del flat_pat_df['abo_r']


    # one hot encoding sex var
    for sex in ['F', 'H']:
        if 'sex_r' in flat_pat_df.columns:
            flat_pat_df.loc[:, 'sex_r_' + sex] = (flat_pat_df['sex_r']==sex)
        if 'sex_d' in flat_pat_df.columns:
            flat_pat_df.loc[:, 'sex_d_' + sex] = (flat_pat_df['sex_d']==sex)
    # delete old representation of sex
    if 'sex_r' in flat_pat_df.columns:
        del flat_pat_df['sex_r']
    if 'sex_d' in flat_pat_df.columns:
        del flat_pat_df['sex_d']

    if 'siav' in flat_pat_df.columns:
        flat_pat_df.loc[flat_pat_df['siav'].isnull(), 'siav'] = False
        flat_pat_df.loc[flat_pat_df['siav'].astype(str)=='B', 'siav'] = True

    return flat_pat_df


def convert_to_numerical(patient_df):
    num_pat_df = patient_df.copy()

    num_pat_df['dt_probnp'] = num_pat_df['dt_probnp'].apply(lambda x: time.mktime(x.timetuple()))
    num_pat_df['dt_bnp'] = num_pat_df['dt_bnp'].apply(lambda x: time.mktime(x.timetuple()))
    num_pat_df['dt_creat'] = num_pat_df['dt_creat'].apply(lambda x: time.mktime(x.timetuple()))
    num_pat_df['dt_bili'] = num_pat_df['dt_bili'].apply(lambda x: time.mktime(x.timetuple()))

    if 'delay_probnp' in num_pat_df.columns:
        num_pat_df['delay_probnp'] = num_pat_df['delay_probnp'].apply(lambda x: x.days)
    if 'delay_bnp' in num_pat_df.columns:
        num_pat_df['delay_bnp'] = num_pat_df['delay_bnp'].apply(lambda x: x.days)
    if 'delay_la' in num_pat_df.columns:
        num_pat_df['delay_la'] = num_pat_df['delay_la'].apply(lambda x: x.days)
    if 'delay_bili' in num_pat_df.columns:
        num_pat_df['delay_bili'] = num_pat_df['delay_bili'].apply(lambda x: x.days)

    return num_pat_df


def preprocess_data(patient_df):
    # data = var_selection(patient_df)
    data = convert_to_numerical(patient_df)
    data = one_hot_encode(data)
    return data
