import yaml
import datetime as dt


with open("./settings/config.yml", 'r') as ymlfile:
    cfg = yaml.load(ymlfile, Loader=yaml.FullLoader)
    cfg['score']['today'] = cfg['score']['today'].date()
    cfg['score']['delay_probnp'] = dt.timedelta(days=cfg['score']['delay_probnp'])
    cfg['score']['delay_bnp'] = dt.timedelta(days=cfg['score']['delay_bnp'])
    cfg['score']['delay_la'] = dt.timedelta(days=cfg['score']['delay_la'])
    cfg['score']['delay_bili'] = dt.timedelta(days=cfg['score']['delay_bili'])
