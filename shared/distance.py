import pandas as pd
import numpy as np
import pdb
import datetime as dt



def compute_feature_similarity_to_x(feature_serie, x, col_std):
    if feature_serie.dtype in ['int16', 'int32', 'int64', 'float16', 'float32', 'float64']:
        # L1 norm
        dist = abs(feature_serie - x) / col_std
        return 1 / (1 + dist)
    if type(feature_serie.head(1).values[0]) in [dt.date]:
        # L1 norm
        dist = abs(feature_serie - x) / col_std
        return 1 / (1 + dist.astype(float))
    if type(feature_serie.head(1).values[0])==set:
        # Jaccard similarity
        return feature_serie.apply(lambda item: len(item.intersection(x)) / float(len(item)))
    else:
        # Goodall similarity
        eq_counts = sum(feature_serie==x)
        N_tot = len(feature_serie)
        return (feature_serie==x) * (eq_counts * (eq_counts - 1)) / (N_tot * (N_tot - 1))


def compute_similarity_to_x(pat_df, x, features_subset, data_std):
    feature_dist_df = pd.DataFrame()
    for col in features_subset:
        feature_dist_df[col] = compute_feature_similarity_to_x(pat_df[col], x[col], data_std.get(col, None))
    return feature_dist_df.mean(axis=1)


def compute_distance_to_x(pat_df, x, features_subset, data_std):
    similarities = compute_similarity_to_x(pat_df, x, features_subset, data_std)
    dist = similarities.apply(lambda x: 1./x - 1 if x!=0 else np.inf)
    return dist
