import itertools
from shared.distance import *


def distance_filter_population(pat_df, x_e, n_closest, feat_subset, data_std):
    """
    Return filters s.t. only the n_closest samples from pat_df  w.r.t features_subset are kept
    """
    similarities = compute_similarity_to_x(pat_df, x_e, feat_subset, data_std)
    sim_threshold = np.partition(similarities, - n_closest)[- n_closest]
    return {'expl_data': similarities >= sim_threshold}


def f_smpl_combine_subset(theta, x_e, x_p):
    """
    Samping function that replaces values of x_p by values of x_e when the feature
    is in theta.
    """
    for col in x_e.keys():
        if col not in theta:
            x_p[col] = x_e[col]
    return x_p


def generate_samples_combine(expl_scope, expl_data, theta_parameter, filters):
    """Fast version of the samples generator in the specific case of f_smpl_combine_subset
    """
    if filters.get('expl_scope', None) is not None:
        expl_scope = expl_scope[filters['expl_scope']].copy()
    if filters.get('expl_data', None) is not None:
        expl_data = expl_data[filters['expl_data']].copy()
    if filters.get('theta_parameter', None) is not None:
        theta_parameter = theta_parameter[filters['theta_parameter']].copy()
    for col in expl_data.keys():
        if col not in theta_parameter:
            if type(expl_scope[0][col])==set:
                for idx in expl_data.index:
                    expl_data.at[idx, col] = expl_scope[0][col]
            else:
                expl_data.loc[:, col] = expl_scope[0][col]
    expl_data = expl_data.reset_index(drop=True)
    expl_data.loc[(expl_data['age_r'] < 18) & (expl_data['urgence']=='XPCA'), 'urgence'] = 'XPCP1'
    expl_data.loc[(expl_data['age_r'] >= 18) & ((expl_data['urgence']=='XPCP1') | (expl_data['urgence']=='XPCP2')), 'urgence'] = 'XPCA'
    return expl_data


def generate_samples(f_smpl, expl_scope, expl_data, theta_parameter, filters):
    if filters.get('expl_scope', None) is not None:
        expl_scope = expl_scope[filters['expl_scope']].copy()
    if filters.get('expl_data', None) is not None:
        expl_data = expl_data[filters['expl_data']].copy()
    if filters.get('theta_parameter', None) is not None:
        theta_parameter = theta_parameter[filters['theta_parameter']].copy()
    return pd.DataFrame(data=[
        f_smpl(theta, x_e, x_p[1])
        for (theta, x_e, x_p) in list(itertools.product(theta_parameter, expl_scope, expl_data.iterrows()))
        ])






#
