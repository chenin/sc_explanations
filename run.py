import pylab as plt

from src.generate_rdm_data import *
from src.counterfactuals_sc import *
from src.compute_score_coeur import *
from src.lime_sc import *
from src.pdp_plot import *

def simulate_new_giver(population_size):
    pat_df = create_rdm_pat_db_one_giver(population_size)
    target_score = max(compute_sc_df(pat_df))
    patient = pat_df.sample(1)
    print("Score of the patient", compute_sc_df(patient).values[0])
    print("Maximum score", target_score)
    giver = patient[['age_d', 'sex_d', 'taille_d', 'poids_d', 'abo_d']].to_dict(orient='records')[0]
    return pat_df, patient, giver, target_score


def best_counterfactuals(nb, pat_df, patient, giver, target_score):
    res = find_counterfactuals(patient, giver, pat_df, target_score,
                               batch_size=(5 * len(pat_df)))
    res = res.head(nb)
    counterfactual_list = []
    for rank, item in enumerate(res.iterrows()):
        cf = {"score": item[1]['score'], "distance": item[1]['distance']}
        for key in patient.keys():
            if item[1][key] != patient[key].values[0]:
                cf[key] = item[1][key]
        counterfactual_list.append(cf)
    txt_expl_list = []
    for cf in counterfactual_list:
        textual_expl = "Actuellement, votre patient a un score de {sc_pat}, le premier de la liste a un score de {target}. "
        textual_expl += "Si votre patient avait eu : \n"
        for idx, (var, val) in enumerate(cf.items()):
            if var not in ['score', 'distance']:
                if idx == len(cf) - 2:
                    textual_expl += "\t{var} = {new_val} au lieu de {var} = {old_val} et \n".format(var=var, new_val=val, old_val=patient[var].values[0])
                else:
                    textual_expl += "\t{var} = {new_val} au lieu de {var} = {old_val}, \n".format(var=var, new_val=val, old_val=patient[var].values[0])
        textual_expl += "alors son score serait de {new_score}.\n\n".format(new_score=cf['score'])
        textual_expl = textual_expl.format(sc_pat=int(compute_sc_df(patient).values[0]), target=int(target_score))
        txt_expl_list.append(textual_expl)
        print(textual_expl)
    return txt_expl_list


def generate_lime_plot(patient, giver, nb_var=10, sigma=.5):
    lime_coef = explain_lime(patient, giver, n_smpl=10000, sigma=sigma)
    k_biggest_lime = dict(sorted(lime_coef.items(),
                                 key=lambda x: abs(x[1]),
                                 reverse=True)[:nb_var])
    # representation of LIME results
    y_pos = np.arange(len(k_biggest_lime))
    lm_coef = list(k_biggest_lime.values())
    labels = list(k_biggest_lime.keys())
    fig = plt.figure(figsize=(14, 7))
    clrs = ['b' if y == 1 else 'r' for y in np.sign(lm_coef)]
    plt.barh(y_pos, lm_coef, align='center', color=clrs, tick_label=labels)
    plt.show()


def interim_score(patient):
    pat_dict = patient.to_dict(orient='records')[0]
    res = {}
    res["snacg"] = f_snacg(pat_dict)
    res["ccp"] = f_ccp(pat_dict)
    res["ccb"] = f_ccb(pat_dict)
    res["icar"] = f_icar(pat_dict)
    return res

def pdp_plots(pat_db, nb_bins_numeric=50):
    data_ranges = infer_data_ranges(pat_db, nb_bins_numeric)
    bins_values = mean_sc(pat_db, data_ranges)
    plot_pdp(data_ranges, bins_values)
    return data_ranges, bins_values
