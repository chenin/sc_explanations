import pdb
import time
import numpy as np
from dataprocess.compute_score_coeur import compute_sc_df
from dataprocess.preprocess import compute_std
from shared.sample import *
import pandas as pd
from settings import *


def generate_counterfactuals(pat_df, x_e, target_score, nb_cf):
    data_std = compute_std(pat_df)
    cf_list = []
    k = int(len(pat_df.columns) / 4)
    for _ in range(10):
        print(_, k)
        S_tot = pd.DataFrame()
        for __ in range(1000):
            start_time = time.time()
            feat_subset = np.random.choice(pat_df.columns, k)
            filters = distance_filter_population(pat_df, x_e, cfg['cf']['n_closest'], feat_subset, data_std)
            S = generate_samples_combine([x_e],
                                         pat_df,
                                         feat_subset,
                                         filters)
            S_tot = pd.concat([S_tot, S], axis=0)
        scores = compute_sc_df(S_tot)
        valid_cf = S_tot[scores > target_score]
        if len(valid_cf) > 0:
            similarities = compute_similarity_to_x(valid_cf, x_e, valid_cf.columns, data_std)
            for sim, cf in zip(similarities, valid_cf.iterrows()):
                if len(cf_list) < nb_cf:
                    cf_list.append((_, sim, cf[1]))
                    cf_list.sort(key=lambda x: -x[1])
                elif sim > cf_list[-1][1]:
                    cf_list = cf_list[:-1] + [(_, sim, cf[1])]
                    cf_list.sort(key=lambda x: -x[1])
            k = int(k / 2)
        else:
            k = min(int(k * 2), len(pat_df.columns))
    return cf_list
