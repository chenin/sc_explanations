import numpy as np
from sklearn.linear_model import Ridge, LinearRegression
from sklearn.model_selection import GridSearchCV
from sklearn.pipeline import Pipeline
from shared.sample import *
from shared.distance import *
from dataprocess.preprocess import *
from dataprocess.compute_score_coeur import *


def w(distances, sigma):
    """Function to compute sample weight according to distance between points
    """
    res = 1. / np.sqrt(2 * np.pi * sigma)
    res *= np.exp((-distances**2) / (2 * sigma**2))
    return res


def explain_lime(x_e, pat_df, n_smpl=500, sigma=0.5):
    data_std = compute_std(pat_df)
    S_tot = pd.DataFrame()
    for _ in range(n_smpl):
        k = np.random.choice(len(pat_df.columns))
        feat_subset = np.random.choice(pat_df.columns, k)
        S = generate_samples_combine([x_e], pat_df.sample(1), feat_subset, {})
        S_tot = pd.concat([S_tot, S], axis=0)
    distances = compute_distance_to_x(S_tot, x_e, pat_df.columns, data_std)
    weights = w(distances, sigma)
    weights = weights / sum(weights)
    print("Number of weights >10**-9: ", sum(np.array(weights)>10**-9))
    print("Number of weights >10**-6: ", sum(np.array(weights)>10**-6))
    print("Number of weights >10**-3: ", sum(np.array(weights)>10**-3))
    print("Number of weights >10**-1: ", sum(np.array(weights)>10**-1))
    mod_predictions = compute_sc_df(S_tot)
    linear_mod = Ridge()
    parameters = {'lr__alpha': np.logspace(-2, 6, 100)}
    complete_model = Pipeline([('lr', linear_mod)])
    clf = GridSearchCV(complete_model, parameters, cv=5)
    pat_df_flat = preprocess_data(S_tot)
    clf.fit(pat_df_flat, mod_predictions, lr__sample_weight=weights)
    # print(clf.best_params_)
    coef_dict = {key: val for (key, val) in zip(pat_df_flat.columns, clf.best_estimator_.steps[-1][-1].coef_)}
    # coef_dict = {key: val * expl_flat[x] for (key, val, x) in
    #              zip(db_flat.columns, clf.best_estimator_.steps[-1][-1].coef_, expl_flat) if type(expl_flat[x]) == bool}
    return coef_dict
