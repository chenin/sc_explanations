import datetime as dt
import numpy as np
import pylab as plt
import pdb

from src.generate_rdm_data import *
from src.compute_score_coeur import *


def infer_data_ranges(pat_db, nb_num_bins):
    range_dict = {}
    for col in pat_db.columns:
        # Numeric types
        if type(pat_db.loc[0, col]) in [np.int64, np.float64]:
            range_dict[col] = np.round(np.linspace(min(pat_db[col]),
                                          max(pat_db[col]),
                                          num=nb_num_bins), 1)
        # date type
        elif type(pat_db.loc[0, col]) == dt.date:
            bin_size = (max(pat_db[col]) - min(pat_db[col])).days / float(nb_num_bins)
            range_dict[col] = [min(pat_db[col]) + dt.timedelta(days=round((1 + x) * bin_size))
                               for x in range(0, nb_num_bins)]
        # not numeric types
        else:
            range_dict[col] = pat_db[col].unique()
    return range_dict


def mean_sc(pat_db, range_dict):
    values_dict = {}
    for col in pat_db.columns:
        new_db = pat_db.copy()
        val_array = []
        print("Computing var :", col)
        for val in range_dict[col]:
            new_db.loc[:, col] = val
            scores = compute_sc_df(new_db, safe=True)
            val_array.append(np.nanmean(scores))
        values_dict[col] = val_array
    return values_dict


def plot_pdp(range_dict, values_dict):
    for col in range_dict.keys():
        x_array = range(len(range_dict[col]))
        plt.plot(x_array, values_dict[col], '-o')
        plt.xticks(np.array(x_array), tuple(range_dict[col]), rotation=90)
        plt.title(col)
        plt.savefig("PDP_score_coeur/" + col + ".png", bbox_inches='tight')
        plt.show()